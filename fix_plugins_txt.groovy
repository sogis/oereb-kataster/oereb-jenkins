@Grab('com.xlson.groovycsv:groovycsv:1.3')
import static com.xlson.groovycsv.CsvParser.parseCsv
 
fh = new File('plugins.txt')
def csv_content = fh.getText('utf-8')
 
def data_iterator = parseCsv(csv_content, separator: '|', readFirstLine: true)
// println data_iterator.getClass()  // class com.xlson.groovycsv.CsvIterator
for (line in data_iterator) {
    println line[0].trim()+":"+line[1].trim()
}
