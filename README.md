# oereb-jenkins

## Preconfigure plugins
Get list of installed plugins (for Dockerfile). Go to http://localhost:8080/script, then:
```
jenkins.model.Jenkins.instance.getPluginManager().getPlugins().stream().sorted().each { println "${it.getShortName()}:${it.getVersion()}" }
```
Save list as `plugins.txt`.

## Building and running Jenkins

```
docker build -t sogis/oereb-jenkins .
```

```
docker run -p 8080:8080 -p 50000:50000 \
-e ORG_GRADLE_PROJECT_dbUriOereb='jdbc:postgresql://host.docker.internal:54321/oereb' \
-e ORG_GRADLE_PROJECT_dbUserOereb='gretl' \
-e ORG_GRADLE_PROJECT_dbPwdOereb='gretl' \
-v jenkins_home:/var/jenkins_home sogis/oereb-jenkins
```

## Varia
- Manuell Plugins wählen. Alle abwählen. Weiter...

