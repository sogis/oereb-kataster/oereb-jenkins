FROM jenkins/jenkins:lts
COPY plugins.txt /usr/share/jenkins/ref/plugins.txt
RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.txt

HEALTHCHECK --interval=30s --timeout=10s --start-period=60s CMD curl http://localhost:8080 

